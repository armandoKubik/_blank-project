<?php
	session_start();
	include 'inc/config.php';
	//include 'inc/globals.php';
	include 'inc/metatags.php';
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="icon" href="favicon.png">
		<link rel="apple-touch-icon" href="apple-touch-icon-180px.png" />
	    <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72px.png" />
	    <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114px.png" />
	    <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144px.png" />
		<title><?php echo($titulo); ?></title>
		<meta name="description" content="<?php echo($descripcion); ?>">
		<meta name="keywords" content="<?php echo($keywords); ?>">
		<!-- <meta name="copyright" content=""> -->
		<meta name="author" content="kubik Digital Agency" />
		<meta name="robots" content="index, follow">
		<meta name="googlebot" content="index, follow">
		<meta http-equiv="content-language" content="ES">
		<meta name="Rating" content="General">
		<!-- OPENGRAPH -->
		<meta property="og:title" content="<?php echo($titulo); ?>">
		<meta property="og:type" content="website">
		<meta property="og:url" content="<?php echo($url); ?>">
		<meta property="og:image" content="<?php echo($fb_img); ?>"/>
		<meta property="og:image:type" content="image/jpeg" />
		<meta property="og:image:width" content="1200">
		<meta property="og:image:height" content="630">
		<meta property="og:description" content="<?php echo($descripcion); ?>">
		<!-- TWITTER CARD -->
		<meta name="twitter:card" content="summary">
		<meta name="twitter:site" content="">
		<meta name="twitter:title" content="<?php echo($titulo); ?>">
		<meta name="twitter:description" content="<?php echo($descripcion); ?>">
		<meta name="twitter:image:src" content="<?php echo($fb_img); ?>">
		<!-- COLOR NAVEGADOR -->
    	<meta name="theme-color" content="#2d6966" />
		<!-- BOOTSTRAP CORE CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- FONT AWESOME -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/solid.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/brands.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/fontawesome.css">
		<!-- OWL CAROUSEL -->
		<link rel="stylesheet" href="css/owl.carousel.min.css">		
	    <!-- MODERNIZR -->
	    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
		<!-- ADITIONAL GOOGLE-FONT -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,500,700" rel="stylesheet">
		<!-- CUSTOM CSS -->
		<link href="css/animate.css" rel="stylesheet">
		<link rel="stylesheet" href="css/main.css">
	</head>

  <body>

	<?php require_once "sections/tpl/header.php"; ?>

    <?php 
		if(file_exists("sections/".$section.".php")){
			require_once "sections/".$section.".php";
		}else{
			require_once "sections/404.php";
		}
	?>

    <?php require_once "sections/tpl/footer.php"; ?>

	<!-- JQUERY -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-3.2.1.slim.min.js"><\/script>')</script>
	<!-- BOOTSTRAP -->
	<script src="js/popper.min.js" ></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <!-- SCROLLMAGIC -->
    <script type="text/javascript" src="js/lib/greensock/TweenMax.min.js"></script>
    <script src="js/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script type="text/javascript" src="js/scrollmagic/uncompressed/plugins/animation.gsap.js"></script>
    <!-- OWL CAROUSEL -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- EASING -->
    <!--<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>-->
    <!-- CUSTOM -->
    <script src="js/main.js"></script>
	<?php if($section == 'home'){echo '<script src="js/home.js"></script>';} ?>
    <!-- GOOGLE ANALYTICS -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>

  </body>
</html>