<style>
    section#notFound404{min-height:860px;padding:0;position:relative;}
    section#notFound404:after{content:'';position:absolute;width:100%;height:175px;background:#fff;top:0;left:0;}
    * {line-height: 1.2;margin:0;}
    h1 {color:#555;font-size:2em;font-weight:400;}
    p {margin:0 auto;}
    @media only screen and (max-width: 280px) {
        h1 {font-size: 1.5em;margin: 0 0 0.3em;}
    }
    .navbar-height{height:100px;}
</style>


<section class="container-fluid" id="notFound404">
    <div class="container" style="text-align:center;">
        <div class="navbar-height"></div>
        <div class="clearfix" style="height:360px;"></div>
        <h1>Error 404 (page not found)</h1>
        <p>Sorry for the inconvenience.</p>
    </div>
</section>
