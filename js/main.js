$(document).ready(function() {

  // para cerrar el navbar al click
  $('.mainMenuBtn','.special-btn').on('click', function(){
      if ( $( '.navbar-collapse' ).hasClass('in') ) {
          $('.navbar-toggle').click();
      }
  });
  $('.navbar-brand').on('click', function(){
      if ( $( '.navbar-collapse' ).hasClass('in') ) {
          $('.navbar-toggle').click();
      }
  }); 

});

/// scroll-navigation  
$(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        if($('.navbar-toggle').css('display') != 'none')
          $('.navbar-toggle').trigger( "click" );
        
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: (target.offset().top *1) - ($("#mainMenu .navbar-header").height()*1)
          }, 1000, 'easeInOutExpo');
          return false;
        }
      }
    });
});
